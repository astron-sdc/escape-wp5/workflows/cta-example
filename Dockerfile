FROM python:3.9-slim
RUN pip install --no-cache notebook jupyterlab

RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

RUN python3.9 -m pip install --no-cache-dir --upgrade \
    pip \
    setuptools \
    wheel
#RUN python3.9 -m pip install --no-cache-dir gammapy==0.19 matplotlib==3.4.3 numpy==1.21.4 iminuit==2.8.4
RUN python3.9 -m pip install --no-cache-dir matplotlib==3.5.2 iminuit==2.11.2 gammapy==0.20.1 numpy==1.22.3

ARG NB_USER=gammapy
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}

# Make sure the contents of our repo are in ${HOME}
COPY data/ ${HOME}/data
COPY caldb/data/cta/prod5-v1 ${HOME}/caldb/data/cta/prod5-v1
COPY analysis.ipynb ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

